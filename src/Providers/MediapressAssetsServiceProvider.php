<?php

namespace Mpassets\Providers;

use Illuminate\Support\ServiceProvider;

class MediapressAssetsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../Assets' => public_path('vendor/mediapress'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
