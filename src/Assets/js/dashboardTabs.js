Highcharts.chart('visit-tab', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: window.graphData.tabs.visits.categories
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' '
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            color: '#FED300',
            fillOpacity: 0.3
        }
    },
    series: [{
        name: 'Visits',
        data: window.graphData.tabs.visits.values
    }]
});

Highcharts.chart('session-tab', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: window.graphData.tabs.sessions.categories
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' '
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            color: '#39D2FF',
            fillOpacity: 0.3
        }
    },
    series: [{
        name: 'Sessions',
        data: window.graphData.tabs.sessions.values
    }]
});

Highcharts.chart('cookie-tab', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: window.graphData.tabs.cookies.categories
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' '
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            color: '#A3A0FB',
            fillOpacity: 0.3
        }
    },
    series: [{
        name: 'Cookies',
        data: window.graphData.tabs.cookies.values
    }]
});
