function openBackupModal(type, id) {
    $('#backup-modal').modal('show');

    $('#backup-modal .modal-content').html('<div class="modal-header">\n' +
        '                <h4 class="modal-title">Geçmiş Sürümler</h4>\n' +
        '\n' +
        '                <button type="button" class="close" id="closePopup" data-dismiss="modal" aria-hidden="true" onclick="javascript:close_backup(this);">×\n' +
        '                </button>\n' +
        '            </div><div class="modal-body">\n' +
        '\n' +
        '            </div>');

    $.get(location.protocol + '//' + location.host + '/mp-admin/ajax/backup?type=' + type + '&id=' + id + '&_=' + (new Date()).getTime(), function (response) {
        $('#backup-modal .modal-body').html(response);
    });
}

function loadBackup(el) {
    let content = el.data('content');

    if (isJSON(content)) {
        $.each(content, function (i, val) {
            let name = '[name="' + i + '"]';
            let dataf = undefined;
            if (isJSON(val) && val != null) {
                $.each(val, function (f, value) {
                    name = '[name="' + i + '[' + f + ']"]';
                    dataf = $(name).data('backup-function');
                    callBackup(dataf, name, value);
                })
            } else {
                dataf = $(name).data('backup-function');
                callBackup(dataf, name, val);
            }

            $('#backup-modal').modal('hide');
        });
    } else {
        //TODO give error
    }

}

function isJSON(something) {
    if (typeof something != 'string'){
        something = JSON.stringify(something);
    }

    if(something == parseInt(something)){
        return false;
    }
    try {
        JSON.parse(something);
        return true;
    } catch (e) {
        return false;
    }
}

function callBackup(dataf, name, val) {
    if (dataf !== undefined) {
        window[dataf](name, val);
    } else {
        $(name).val(val);
    }
}

function backupFiles(name,value) {
    $(name).val(value);
    let id = $(name).parents('.post-file').attr('id');
    let key = $(name).parents('.post-file').attr('data-key');
    handleFileManagerSelection(id,JSON.parse(value), key);
}

function backupCKEditor(name,value) {
    $(name).html(value);
    $(name).val(value);
    var next = $(name).next();
    if (next.hasClass('cke')) {
        CKEDITOR.instances[$(name).attr('id')].setData(value);
    }

}

function close_backup(el) {
    el.closest('#backup-modal').modal('hide');
}


$('.modal').on("hidden.bs.modal", function (e) {
    if($('.modal:visible').length)
    {
        $('.modal-backdrop').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) - 10);
        $('body').addClass('modal-open');
    }
}).on("show.bs.modal", function (e) {
    if($('.modal:visible').length)
    {
        $('.modal-backdrop.in').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) + 10);
        $(this).css('z-index', parseInt($('.modal-backdrop.in').first().css('z-index')) + 10);
    }
});
