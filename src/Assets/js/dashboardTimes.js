Highcharts.chart('pieTimes', {

    chart: {
        type: 'heatmap',
        plotBorderWidth: 0
    },


    title: {
        text: ''
    },

    xAxis: {
        categories: ['Paz', 'Pzt', 'Sal', 'Çar', 'Per','Cum','Cmt']
    },

    yAxis: {
        align:'right',
        categories: ['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23'],
        title: null,
        reversed: true
    },

    colorAxis: {
        min: 0,
        minColor: '#e6ebf5',
        maxColor: '#203499'
    },
    legend: {
        align: 'right',
        layout: 'vertical',
        margin: 0,
        verticalAlign: 'top',
        y: 25,
        symbolHeight: 280
    },

    tooltip: {
        formatter: function () {
            return '<b>' + this.series.xAxis.categories[this.point.x] + '<br></b><b>' +
                this.point.value + '</b>  on <b>' + this.series.yAxis.categories[this.point.y] + '</b>';
        }
    },
    series: [{
        name: '',
        data: window.graphData.hourly,
        dataLabels: {
            enabled: false,
            color: '#fff'
        }
    }]

});
