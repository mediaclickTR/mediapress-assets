(function ($) {
    $.fn.passwordStrength = function (options) {
        var defaults = $.extend({
            minimumChars: 6
        }, options);

        var parentContainer = this.parent();
        var progressHtml = "<div class='progress'>" +
            "<div id='password-progress' class='progress-bar' role='progressbar' aria-valuenow='1' aria-valuemin='0' aria-valuemax='100' style='width:1%;'></div>" +
            "</div><div id='password-score' class='password-score'></div><div id='password-recommendation' class='password-recommendation'></div>" +
            "<input type='hidden' id='password-strength-score' value='0'>";

        $(progressHtml).insertAfter('input[type=password]:last');
        $('#password-score').text(defaults.defaultMessage);
        $('.progress').hide();
        $('#password-score').hide();

        $(this).keyup(function (event) {
            $('.progress').show();
            $('#password-score').show();

            var element = $(event.target);
            var password = element.val();

            if (password.length == 0) {
                $('#password-score').html('');
                $('#password-recommendation').html('');

                $('.progress').hide();
                $('#password-score').hide();
                $('#password-strength-score').val(0);
            }
            else {
                var score = calculatePasswordScore(password, defaults);
                $('#password-strength-score').val(score);
                $('.progress-bar').css('width', score + '%').attr('aria-valuenow', score);

                $('#password-recommendation').css('margin-top', '10px');

                if (score < 50) {
                    $('#password-score').html('Weak Password');
                    $('#password-recommendation').html('<ul>' +
                        '<li>Use at least 6 characters</li>' +
                        '<li>Use upper and lower case characters</li>' +
                        '</ul>');
                    $('#password-progress').removeClass();
                    $('#password-progress').addClass('progress-bar progress-bar-danger');
                }
                else if (score <= 60) {
                    $('#password-score').html('Normal password <span class="fa fa-ok" aria-hidden="true"></span>');
                    $('#password-recommendation').html('' +
                        '<div id="password-recommendation-heading">For a stronger password :</div>' +
                        '<ul><li>Use upper and lower case characters</li>' +
                        '</ul>');
                    $('#password-recommendation-heading').css('text-align', 'center');
                    $('#password-progress').removeClass();
                    $('#password-progress').addClass('progress-bar progress-bar-warning');
                }
                else {
                    $('#password-score').html('Very strong password <span class="fa fa-ok" aria-hidden="true"></span>');
                    $('#password-recommendation').html('');
                    $('#password-progress').removeClass();
                    $('#password-progress').addClass('progress-bar progress-bar-success');
                }
            }

        });
    };

    function calculatePasswordScore(password, options) {

        var score = 0;
        var hasNumericChars = false;
        var hasMixedCase = false;

        if (password.length < 1)
            return score;

        if (password.length < options.minimumChars)
            return score;

        //match numbers
        if (/\d+/.test(password)) {
            hasNumericChars = true;
            score += 20;

            var count = (password.match(/\d+?/g)).length;
            if (count > 1) {
                //apply extra score if more than 1 numeric character
                score += 10;
            }
        }

        //mixed case (upper & lower case)
        if ((/[a-z]/.test(password)) && (/[A-Z]/.test(password))) {
            hasMixedCase = true;
            score += 20;
        }

        if (password.length >= options.minimumChars) {
            score += 20;
        }
        if (password.length >= options.minimumChars && hasNumericChars) {
            score += 10;
        }
        if (password.length >= options.minimumChars && hasNumericChars && hasMixedCase) {
            score += 10;
        }
        if (score > 100){
            score = 100;
        }

        return score;
    }
}(jQuery));