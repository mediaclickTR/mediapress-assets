window.mediapress = window.location.protocol + '//' + document.domain + '/mp-admin/';

function popup(url, data) {
    $('.pageloading').show();
    var gif = $('.pageloading').html();
    $(".main-modal .modal-content").html(gif);
    $.post(window.mediapress + 'ajax/popup/' + url, {
        _token: $("input[name='_token']").val(),
        data: data
    }, function (response) {
        $('.main-modal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $(".main-modal .modal-content").html(response);
    }).done(function () {
        setTimeout(function () {
            $('.pageloading').hide();
        }, 50);
    });
}

function close_popup(el)
{
    el=$(el);
    var modal = el.closest(".main-modal");
    var modal_content = el.parent();
    $(modal).modal('hide');
    $(".main-modal > div > .modal-content").html(null);
}

function redirect(url) {
    $(location).attr('href', url);
}


