let mFile = function (htmlObject) {

    let root = this;

    let input = null;

    let id = null;

    let tags = null;

    let name = null;

    let values = null;

    let holder = null;


    this.construct = function (htmlObject) {
        this.input = htmlObject;
        this.id = htmlObject.attr('id');
        this.tags = htmlObject.data('tags');
        this.name = htmlObject.attr('name');
        this.values = JSON.parse(htmlObject.val());
        $('<div id="' + this.id + '" class="upload-tab form-group col col-12"></div>').insertBefore(this.input);
        this.holder = $('#' + this.id);
        this.input.remove();

        $.each(this.tags, function (key, values) {
            root.buildParts(key, values, root.name);
        });
    };

    this.buildParts = function (key, values, name) {

        let element_id = root.generate_id();
        let selection = root.getValues(key);

        let output = '' +
            '<div class="form-group col col-12 post-file" id="' + element_id + '" data-key="' + key + '" data-options=\'' + root.json(values) + '\'>\n' +
            '    <b>' + values.title + ' <a href="javascript:void(0);" onclick="javascript:addImage($(this))" class="btn btn-primary" role="button">Ekle </a></b>\n' +
            '    <sub>';
        if (values.files_max != "") {
            output += 'En fazla ' + values.files_max + ' adet yüklenebilir, ';
        }

        if (values.filesize_max != "") {
            output += 'Max dosya boyutu ' + values.filesize_max + ' KB, ';
        }

        output += 'Kabul edilen formatlar: ' + values.extensions + '</sub>\n' +
            '  <input name="' + name + '->syncMFiles[' + key + ']" type="hidden" data-backup-function="backupFiles" value="' + selection + '">\n' +
            '    <div class="slidepage row mb0 upload-list">\n' +
            '        <ul class="sortable">\n' +

            '        </ul>\n' +
            '    </div>\n' +
            '</div>';
        this.holder.append(output);

        handleFileManagerSelection(element_id, JSON.parse(selection), key);
    };

    this.getValues = function (key) {

        if (root.values[key] != undefined) {
            return root.json(root.values[key]);
        }
        return root.json([]);
    };

    this.json = function (object) {
        let string = JSON.stringify(object);

        return string.replace("/\"/g", '&quot;');
    };

    this.generate_id = function () {
        //edit the token allowed characters
        var a = "abcdef1234567890".split("");
        var b = [];
        for (var i = 0; i < 11; i++) {
            var j = (Math.random() * (a.length - 1)).toFixed(0);
            b[i] = a[j];
        }
        return "mf" + b.join("");
    };
    this.construct(htmlObject);

};

function insertFiles(data, element_id) {
    let sortable = $('#' + element_id).find('.upload-list > ul');
    let language_tab = $('#' + element_id).closest('.has-detail-in.active');
    sortable.html("");

    if (data.image != null) {
        $.each(data.image, function (i, value) {
            //console.log(value);
            let imgsrc = '';
            if (value.type == 'image') {
                imgsrc = value.url;
            } else {
                imgsrc = '//visualpharm.com/assets/369/Image%20File-595b40b85ba036ed117dc1b3.svg';
            }
            let html =
                ' <li class="ui-state-default data-holder open-img" data-id="' + value.id +  '">\n' +
                getHtmlImages(value, imgsrc) +
                getHtmlModalView(value, imgsrc) +
                getHtmlModalRotate(value, imgsrc) +
                getHtmlModalCrop(value, imgsrc) +
                ' <form method="post" id="file_' + value.id + value.file_key + '">' +
                getHtmlModalEdit(value, imgsrc, data.languages, element_id) +
                '</form>' +
                '</li>';

            sortable.append(html);
        });
    }
    $('#' + element_id + ' .sortable').sortable({
        update: function (event, ui) {
            setImages(element_id);
        }
    });
    setImages(element_id);


}

function getHtmlImages(image, src) {

    var html =
        '<div class="images">\n' +
        '    <div class="img">\n' +
        '       <div class="imag">\n' +
        '           <img src="' + src + '" alt="">\n' +
        '       </div>\n' +
        '    </div>\n' +
        '</div>\n' +
        '<div class="option">\n' +
        '    <u>' + image.name + '</u>\n' +
        '    <span onclick="optionUp($(this));"><img src="/vendor/mediapress/images/option.png" alt=""></span>\n' +
        '    <ul  style="display: none">\n' +
        '        <li><a data-toggle="modal" href="#goruntuleModal_' + image.id + "_" + image.file_key + '"> <img src="/vendor/mediapress/images/gor.png" alt="">Görüntüle </a></li>\n' +
        '        <li><a data-toggle="modal" href="#duzenleModal_' + image.id + "_" + image.file_key + '"> <img src="/vendor/mediapress/images/duzenle.png" alt="">Düzenle </a></li>\n';
    if (image.type == 'image') {
        html +=
            '<li>' +
            '   <a data-toggle="modal" href="#dondurModal_' + image.id + "_" + image.file_key + '">' +
            '       <img src="/vendor/mediapress/images/right-rotate.png" alt=""> Döndür' +
            '   </a>' +
            '</li>\n' +
            '<li>' +
            '   <a data-toggle="modal" href="#kirpModal_' + image.id + "_" + image.file_key + '"class="crop-btn" data-id="' + image.id + '" >' +
            '       <img src="/vendor/mediapress/images/kirp.png" alt=""> Kırp' +
            '   </a>' +
            '</li>\n';
    }
    html +=
        '        <li><a href="javascript:void(0);" onclick="javascript:removeImage($(this))"> <img src="/vendor/mediapress/images/sil.png" alt="">Kaldır </a></li>\n' +
        '    </ul>\n' +
        '</div>\n';
    return html;

}

function getHtmlModalView(image, src) {

    var html =
        '<div class="modal uploadsModal fade" id="goruntuleModal_' + image.id + "_" + image.file_key + '" tabindex="-1" role="dialog" aria-labelledby="goruntuleModalLabel_' + image.id + '" aria-hidden="true">\n' +
        '    <div class="modal-dialog" role="document">\n' +
        '        <div class="modal-content">\n' +
        '            <div class="modal-bg">\n' +
        '            <button type="button" class="close" data-dismiss="modal" aria-label="Close">Kapat</button>\n' +
        '            <div class="modal-body text-center">\n' +
        '                <div class="head">Görüntüle</div>\n' +
        '                <div class="image-page">\n' +
        '                    <img src="' + src + '" alt="">\n' +
        '                    <ul class="image-bilgi">\n' +
        '                        <li>' + image.name + '</li>\n';
    if (image.type == 'image') {
        html += '<li>Ölçü : ' + image.width + ' x ' + image.height + '</li>\n';
    }
    html +=
        '                        <li>Boyut : ' + image.size + '</li>\n' +
        '                    </ul>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';
    return html;
}

function getHtmlModalEdit(image, src, languages, element_id) {
    var language_tab_header = "";
    var language_tab_content = "";
    let language_tab = $('#' + element_id).closest('.has-detail-in');
    let language_tab_id = language_tab.attr('data-language-id');

    $.each(languages, function (i, value) {

        if(language_tab_id != undefined && language_tab_id != value.id) {
            return true;
        }
        var detail = JSON.parse(image.detail) ? JSON.parse(image.detail)[value.id] : [];
        var fullname = detail.fullname != null ? detail.fullname : image.name;
        var title = detail.title != null ? detail.title : "";
        var caption = detail.caption != null ? detail.caption : "";
        var tag = detail.tag != null ? detail.tag : "";

        if (i == 0 || (language_tab_id != undefined && language_tab_id == value.id)) {
            language_tab_header += '<li><a class="show active" data-toggle="tab" id="' + i + '" href="#lang' + image.id + value.id + image.file_key + '">' + value.code.toUpperCase() + '</a></li>\n';
            language_tab_content += '<div id="lang' + image.id + value.id + image.file_key + '" class="tab-pane fade in active show activess" data-id="' + value.id + '">\n';
        } else {
            language_tab_header += '<li><a data-toggle="tab" id="' + i + '" href="#lang' + image.id + value.id + image.file_key + '">' + value.code.toUpperCase() + '</a></li>\n';
            language_tab_content += '<div id="lang' + image.id + value.id + image.file_key + '" class="tab-pane fade show" data-id="' + value.id + '">\n';
        }
        language_tab_content +=
            ' <div class="row mt-2">\n' +
            '    <div class="col-7">\n' +
            '        <div class="form-group row">\n' +
            '            <label for="filename" class="col-sm-3 col-form-label">Dosya Adı</label>\n' +
            '            <div class="col-sm-9">\n' +
            '                <input type="text" class="form-control" name="detail[' + image.id + '][' + value.id + '][fullname]" value="' + fullname + '">\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="form-group row">\n' +
            '            <label for="title" class="col-sm-3 col-form-label">Title</label>\n' +
            '            <div class="col-sm-9">\n' +
            '                <input type="text" class="form-control" name="detail[' + image.id + '][' + value.id + '][title]" value="' + title + '">\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="form-group row">\n' +
            '            <label for="caption" class="col-sm-3 col-form-label">Caption</label>\n' +
            '            <div class="col-sm-9">\n' +
            '                <input type="text" class="form-control" name="detail[' + image.id + '][' + value.id + '][caption]" value="' + caption + '">\n' +
            '            </div>\n' +
            '        </div>\n' +
            '        <div class="form-group row">\n' +
            '            <label for="alttext" class="col-sm-3 col-form-label">Tag</label>\n' +
            '            <div class="col-sm-9">\n' +
            '                <input type="text" class="form-control" name="detail[' + image.id + '][' + value.id + '][tag]" value="' + tag + '">\n' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>\n' +
            '</div>\n';
    });

    var html =
        '<div class="modal uploadsModal fade" id="duzenleModal_' + image.id + "_" + image.file_key + '" tabindex="-1" role="dialog" aria-labelledby="duzenleModalLabel_' + image.id + '" aria-hidden="true">\n' +
        '    <div class="modal-dialog" role="document">\n' +
        '        <div class="modal-content">\n' +
        '            <div class="modal-bg">\n' +
        '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Vazgeç</button>\n' +
        '                <div class="modal-body text-center">\n' +
        '                    <div class="head">Düzenle</div>\n' +
        '                    <p><small>Bu alan için değerlerden birini girip yeniden boyutlandırabilirsiniz.</small></p>\n' +
        '                             <div class="row">' +
        '                                <div class="col-5">\n' +
        '                                    <div class="images rotate-img mb-3"><img src="' + src + '" alt=""></div>\n' +
        '                                    <table class="image-details' + image.id + '">\n' +
        '                                        <tr><td>Dosya Adı:</td><td>:</td><td>' + image.name + '</td></tr>\n' +
        '                                        <tr><td>Dosya Tipi</td><td>:</td><td class="mimetype">' + image.mimetype + '</td></tr>\n' +
        '                                        <tr><td>Yükleme</td><td>:</td><td class="date">' + image.date + '</td></tr>\n' +
        '                                        <tr><td>Yükleyen</td><td>:</td><td class="user">' + image.user + '</td></tr>\n' +
        '                                        <tr><td>Boyut</td><td>:</td><td class="size">' + image.size + '</td></tr>\n';
    if (image.type == 'image') {
        html +=
            '<tr>' +
            '   <td>Ölçüler</td><td>:</td>' +
            '   <td class="width-height">' + image.width + ' × ' + image.height + '</td>' +
            '</tr>\n';
    }
    html +=
        '    </table>\n' +
        '</div>\n';
    if (image.type == 'image') {
        html +=
            '<div class="col-7">\n' +
            '    <div class="form-group row">\n' +
            '        <label class="col-sm-3 col-form-label">Genişlik</label>\n' +
            '        <div class="col-sm-2 pr-0">\n' +
            '            <input type="text" name="width[' + image.id + ']" class="form-control"  value="' + image.width + '">\n' +
            '        </div>\n' +
            '        <div class="col-sm-1">\n' +
            '            <div class="crossed">x</div>\n' +
            '        </div>\n' +
            '        <div class="col-sm-2 pl-0">\n' +
            '            <input type="text" name="height[' + image.id + ']" class="form-control" value="' + image.height + '">\n' +
            '        </div>\n' +
            '        <label class="col-sm-3 col-form-label">Yükseklik</label>\n' +
            '    </div>\n' +
            '</div>\n';
    }

    html +=
        '<input type="text" style="display: none;" name="id" class="form-control" value="' + image.id + '">\n' +
        '<input type="text" style="display: none;" name="file_key" value="' + image.file_key + '">\n';

    if(language_tab.length > 0 && language_tab.attr('data-detail-type') != undefined) {
        html +=
            '<input type="text" style="display: none;" name="model_id" value="' + language_tab.attr('data-detail-id') + '">\n' +
            '<input type="text" style="display: none;" name="model_type" value="' + language_tab.attr('data-detail-type') + '">\n';
    } else {
        html +=
            '<input type="text" style="display: none;" name="model_id" value="' + image.model_id + '">\n' +
            '<input type="text" style="display: none;" name="model_type" value="' + image.model_type + '">\n';
    }

    html +=
        '</div>' +
        '<ul class="nav nav-tabs">\n' +
        language_tab_header +
        '</ul>\n' +
        '<div class="tab-content  pl-0 pr-0 shadow-none">\n' +

        language_tab_content +
        '   <button class="btn btn-primary float-right" onclick="javascript:postEdit($(this))" type="button">Kaydet <i class="fa fa-angle-right"></i></button>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';

    return html;
}

function getHtmlModalRotate(image, src) {

    var html =
        '<div class="modal uploadsModal fade" id="dondurModal_' + image.id + "_" + image.file_key + '" tabindex="-1" role="dialog" aria-labelledby="dondurModalLabel_' + image.id + '" aria-hidden="true">\n' +
        '    <div class="modal-dialog" role="document">\n' +
        '        <div class="modal-content">\n' +
        '            <div class="modal-bg">\n' +
        '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Vazgeç</button>\n' +
        '                <div class="modal-body text-center">\n' +
        '                    <div class="head">Döndür</div>\n' +
        '                       <div class="row">' +
        '                            <div class="col-5">\n' +
        '                                <div class="images rotate-img mb-3"><img src="' + src + '" alt=""></div>\n' +
        '                            </div>\n' +
        '                            <div class="col-7">\n' +
        '                                <div class="form-group row">\n' +
        '                                    <div class="col-sm-3"></div>\n' +
        '                                    <div class="col-sm-4 pr-0">\n' +
        '                                        <button class="left-rotate btn" type="button" onclick="javascript:rotate(\'left\')"><img src="/vendor/mediapress/images/left-rotate.png" alt=""> Sola Döndür</button>\n' +
        '                                    </div>\n' +
        '                                    <div class="col-sm-4 pr-0">\n' +
        '                                        <button class="right-rotate btn" type="button" onclick="javascript:rotate(\'right\')"><img src="/vendor/mediapress/images/right-rotate.png" alt=""> Sağa Döndür</button>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                    </div>' +
        '                    <button class="btn btn-primary float-right" data-id="' + image.id + '" onclick="javascript:postRotate($(this))" type="button">Kaydet <i class="fa fa-angle-right"></i></button>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';
    return html;
}

function getHtmlModalCrop(image, src) {

    var html =
        '<div class="modal uploadsModal fade" id="kirpModal_' + image.id + "_" + image.file_key + '" tabindex="-1" role="dialog" aria-labelledby="kirpModalLabel_' + image.id + '" aria-hidden="true">\n' +
        '    <div class="modal-dialog" role="document">\n' +
        '        <div class="modal-content">\n' +
        '            <div class="modal-bg">\n' +
        '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">Vazgeç</button>\n' +
        '                <div class="modal-body text-center">\n' +
        '                    <div class="head">Kırp</div>\n' +
        '                    <img src="' + src + '" alt="" id="crop-file-' + image.id + '">\n' +
        '                    <div class="mt-5">' +
        '                       <button class="btn btn-primary float-right" data-id="' + image.id + '" onclick="javascript:postCrop($(this))" type="button">Kırp <i class="fa fa-angle-right"></i></button>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';

    return html;
}


function optionUp(el) {

    el.parent().find('ul').slideToggle();
}

function setImages(element_id) {

    let obj = [];
    $.each($('#' + element_id + ' .sortable > li'), function (i, li) {
        obj.push($(li).data('id'));
    });

    $('#' + element_id + ' input[type=hidden]').val(JSON.stringify(obj));
    $('#' + element_id + ' input[type=hidden]').value = JSON.stringify(obj);
}

function removeImage(el) {

    swal({
        title: 'Emin misiniz?',
        text: 'Dikkat bu işlem sayfayı kayıt ederseniz geri alınamaz!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Evet',
        cancelButtonText: 'Vazgeç',
        closeOnConfirm: false,
        closeOnCancel: false
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            let holder = el.parents('.data-holder');
            let id = holder.data('id');
            var wrapper = el.closest(".post-file");
            var current_values = JSON.parse($('#' + wrapper.attr("id") + ' input[type=hidden]').val());

            current_values = jQuery.grep(current_values, function (value) {
                return value != id;
            });
            $('#' + wrapper.attr("id") + ' input[type=hidden]').val(JSON.stringify(current_values));
            $('#' + wrapper.attr("id") + ' input[type=hidden]').value = JSON.stringify(current_values);
            holder.remove();
        }
    });
}

$(document).ready(function () {
    let list = $('.mfile');

    $.each(list, function (i) {
        let input = $(list[i]);
        new mFile(input);
    })
});
