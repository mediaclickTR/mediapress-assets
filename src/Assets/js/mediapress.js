+ function (t) {
    t.fn.extend({
        resimatt: function () {
            t(this).each(function () {
                var e = t(this).attr("src"),
                    i = t(this).attr("class"),
                    s = t(this).attr("alt");
                (i = "undefined") && (i = "image"), t(this).before('<div class="resim-att"><div class="resim-att-pre"><div class="resim-thumb"><div class="resim-centered"><img src="' + e + '" alt="' + s + '" class="' + i + '-att" /></div></div></div></div>'), t(this).remove()
            })
        }
    })
}(jQuery);

function resim_sil(id) {
    var yes = confirm('Bu resmi silmek istediğinize emin misiniz?');
}
function switchVerticalTab(evt, cityName) {
    var i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}


function tableImage() {
    setTimeout(function () {
        $('.lineImage').on("mouseenter", function () {
            var image = $(this).attr('data-image');
            var imgw = $(this).attr('data-width');
            var imgh = $(this).attr('data-height');
            $('body').parent().append("<div class='orjgenel'><div class='row'><div class='orj'><img id='lineImg' src=" + image + " /></div></div></div>");
            var mleft = imgw / 2;
            var mtop = imgh / 2;
            $(".orjgenel").css({ "height": imgh, "width": imgw, "margin-left": -mleft, "margin-top": -mtop });
        });
        $(".lineImage").on("mouseleave", function () {
            $(".orjgenel").remove();
        });
    }, 500)
}

$(document).ready(function () {
    tableImage();
    $(".sortable").sortable();
    $(".sortable").disableSelection();

    $('.checkbox').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
    });

    $('.langbox ul li a').click(function () {
        $(this).parent().find('ul').slideToggle();
    });

    $('.onn-off').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
    });

    var rootwidth = $('.rootwizard .nav-pills li').length;
    var a = 100 / rootwidth;
    var roottak = $('.rootwizard .nav-pills li').width(a);

    /*$('.nav-tabs li .text-danger').click(function () {
        $(this).parent().addClass('disabled');
        $(this).removeClass('fa fa-times text-danger');
        $(this).addClass('fa text-primary fa-plus');
    });

    $('.nav-tabs li .fa-plus').click(function () {
        $(this).parent().removeClass('disabled');
        $(this).removeClass('fa text-primary fa-plus');
        $(this).addClass('fa fa-times text-danger');
    });*/

    $('header .menu .left ul li .submenu > li').each(function(){
        if ($(this).length == 1) {
            /*$(this).parent().parent().addClass('position-relative');*/
        }
    });


    $('.onn-off label input').on('ifChecked', function () {
        $(this).parent().parent().parent().next().addClass('on').show().val('').prop('disabled', true);
    });

    $('.onn-off label input').on('ifUnchecked', function () {
        $(this).parent().parent().parent().next().removeClass('on').hide().val('').prop('disabled', false);
    });

    var menuSub = $('header .menu .left > ul >  li > a');
    menuSub.click(function () {
        if ($(this).parent().hasClass('open')) {
            $(this).next().fadeOut();
            $(this).parent().removeClass('open');
            $(this).prev().removeClass('open');
            $('i.bg').height('0');
        } else {
            menuSub.next().fadeOut();
            menuSub.parent().removeClass('open');
            menuSub.prev().removeClass('open');
            $(this).parent().addClass('open');
            $(this).prev().addClass('open');
            $(this).next().fadeIn(750);
            var ha = $(this).parent().find('.submenu').height();
            $('i.bg').height(ha + 65);
        }
    });

    $('.subTop a').click(function () {
        $('.subTop ul').hide();
    });



    $('header .menu .left ul li .submenu li ul li a').click(function () {
        $('.subTop ul').hide();
    });


    $("header").on("mouseleave", function () {
        setTimeout(function () {
            $('i.bg').height('0');
            $('.submenu').stop().fadeOut();
            $('header .menu .left > ul >  li').removeClass('open');
            $('.subTop ul').fadeOut();
        },750);

    });
    var sites = $('.sites ul li > a');
    sites.click(function () {
        if ($(this).parent().hasClass('open')) {
            $(this).next().slideUp();
            $(this).parent().removeClass('open');
            $(this).prev().removeClass('open');
        } else {
            $('header .menu .right .author ul li').removeClass('open');
            $('header .menu .right .author ul li > ul').slideUp();
            sites.next().slideUp();
            sites.parent().removeClass('open');
            sites.prev().removeClass('open');
            $(this).parent().addClass('open');
            $(this).prev().addClass('open');
            $(this).next().slideDown();
        }
    });


    var aut = $('.author ul li > a');
    aut.click(function () {
        if ($(this).parent().hasClass('open')) {
            $(this).next().slideUp();
            $(this).parent().removeClass('open');
            $(this).prev().removeClass('open');
        } else {
            $('header .menu .right .sites ul li').removeClass('open');
            $('header .menu .right .sites ul li > ul').slideUp();
            aut.next().slideUp();
            aut.parent().removeClass('open');
            aut.prev().removeClass('open');
            $(this).parent().addClass('open');
            $(this).prev().addClass('open');
            $(this).next().slideDown();
        }
    });

    /* $("header .menu .left ul li:first-child").on("mouseover", function () {
         $('header i.bg').height(0);
     });*/
    var pleft = $('.pass-box.left .checkbox label');
    pleft.click(function () {
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $('.form-group.form1').hide();
        } else {
            pleft.parent().removeClass('open');
            pleft.prev().removeClass('open');
            $(this).addClass('open');
            $('.form-group.form1').show();
        }
    });

    $('.form-group').click(function () {
        $(this).addClass('focus');
    });

    $('form .form-group input').focus(function () {
        $(this).parent().addClass('focus');
    });


    $('.resim-att').resimatt();

    $(".pass-box li:nth-child(2)").click(function () {
        $(".pass-box .form-group:nth-child(3)").slideDown();
    });
    $(".pass-box li:nth-child(1)").click(function () {
        $(".pass-box .form-group:nth-child(3)").slideUp();
    });



    $('.pass-box.right .checkbox label').click(function () {
        $('.datepicks').slideUp();
    });

    $('.pass-box.right #dates label').click(function () {
        $('.datepicks').slideDown();
    });
    $("#select1").change(function () {
        if ($(this).children(":selected").attr("id") == "sec1") {
            $("#select2").hide();
        } else {
            $("#select2").show();
        }
    });

    $(".all.url input[type='text']").on('keyup', function () {
        var chn = $(this).val();
        $('.boxes h2 a span').html(chn);
    });
    $(".all .title input[type='text']").on('keyup', function () {
        var chn = $(this).val();
        $(this).parent().parent().parent().find('.boxes i').html(chn);
    });
    $(".all .desc textarea").on('keyup', function () {
        var chn = $(this).val();
        $(this).parent().parent().parent().find('.boxes p').html(chn);
    });
    $(".all .description input[type='text']").on('keyup', function () {
        var chn = $(this).val();
        $(this).parent().parent().parent().find('.boxes p').html(chn);
    });

    $('#nestable').nestable({
        group: 0
    });

    /*var book_name = $('input').val();
    if((book_name == "")||(book_name == null)){
        alert('fail');
    }
    else{
        alert(book_name);
    }*/

    $('.remove').click(function () {
        $(this).next().remove();
        $(this).remove();
    });

    $('.option span').click(function () {
        $(this).parent().find('ul').slideToggle();
        $(this).toggleClass('open')
    });

    $('*').click(function (e) {
        if (!$(e.target).is('.ui-state-default') && !$(e.target).is('.ui-state-default *')) {
            $('.option span').removeClass('open');
            $('.option span').parent().find('ul').slideUp();
        }
    });

    $('.option ul li a').click(function () {
        $('.option ul').slideUp();
    });
    $(".datepick").datepicker({
        language: 'tr',
        format: "dd/mm/yyyy",
        dateFormat: "dd/mm/yy",
        altFormat: "yy-mm-dd",
        altField: "#tarih-db",
        monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        dayNamesMin: ["Pa", "Pt", "Sl", "Çrş", "Pe", "Cu", "Ct"],
        firstDay: 1
    });

    $(".url input[type='text']").on('keyup', function () {
        var chn = $(this).val();
        $('.boxes-pro h2 a span').html(chn);
    });
    $(" .title input[type='text']").on('keyup', function () {
        var chn = $(this).val();
        $('.boxes-pro i').html(chn);
    });
    $(".desc textarea").on('keyup', function () {
        var chn = $(this).val();
        $('.boxes-pro p').html(chn);
    });
    $(".description input[type='text']").on('keyup', function () {
        var chn = $(this).val();
        $('.boxes-pro p').html(chn);
    });
    $('.on-off ul li:last-child').click(function () {
        $(this).prev().find('.switch-toggle').addClass('on');
        $(this).prev().find('.switch-toggle').removeClass('off');
        $(this).prev().find("input").val($(this).prev().find('.switch-toggle').data('on')).prop("checked", true).trigger("change");
    });
    $('.on-off ul li:first-child').click(function () {
        $(this).next().find('.switch-toggle').addClass('off');
        $(this).next().find('.switch-toggle').removeClass('on');
        $(this).next().find("input").val($(this).next().find('.switch-toggle').data('off')).prop("checked", true).trigger("change");
    });

    $('.on-off.cat  ul li:last-child').click(function () {
        $('.similar').show();
        $('.url-detail').show();
    });
    $('.on-off.cat  ul li:first-child span').click(function () {
        $('.similar').hide();
        $('.url-detail').hide();
    });

    $('.on-off.lang ul li:last-child').click(function () {
        $('.url-detail').show();
        $('.url-detail-lang').show();
    });
    $('.on-off.lang ul li:first-child span').click(function () {
        $('.url-detail').hide();
        $('.url-detail-lang').hide();
    });

    $('.similar .item .plus-button').click(function () {
        $(".similar .item:first-child").clone().prependTo(".similar").addClass("delete").removeClass('add');
        $('.similar .delete .plus-button').click(function () {
            $(this).parent().remove();
        });
    });

    $('.on-off.seo  ul li:last-child').click(function () {
        $('.accordion').show();
    });
    $('.on-off.seo  ul li:first-child span').click(function () {
        $('.accordion').hide();
    });

    $('.on-off.url  ul li:last-child').click(function () {
        $('.hidden').show();
    });
    $('.on-off.url  ul li:first-child span').click(function () {
        $('.hidden').hide();
    });

    $('.dropify').dropify();

    $(".lang-box .form-group textarea").on('keyup', function () {
        var ssss = $(this).val().length;
        $(this).parent().find('i.note').html(ssss);
    });

    $(".lang-box .form-group:first-child input").on('keyup', function () {
        var wwww = $(this).val().length;
        $(this).parent().find('i.note').html(wwww);
    });


    $('.pop .item:first-child').addClass('open');
    var popupc = $('.pop h3');
    popupc.click(function () {
        if ($(this).parent().hasClass('open')) {
            $(this).next().slideUp();
            $(this).parent().removeClass('open');
            $(this).prev().removeClass('open');
        } else {
            popupc.next().slideUp();
            popupc.parent().removeClass('open');
            popupc.prev().removeClass('open');
            $(this).parent().addClass('open');
            $(this).next().slideDown();
        }
    });

    $('.lang .img').click(function () {
        $(this).parent().find('ul').slideToggle();
    });

    $('.lang ul li img').click(function () {
        var langimg = $(this).attr('src');
        $(this).parent().parent().parent().find('.img img').attr('src', langimg);
        $('.lang ul').slideUp();
    });

    $('.social-map .plus-button').click(function () {
        $(".social-map .item:first-child").clone().prependTo(".social-map form").addClass("delete").removeClass('add');
        $('.social-map .delete .plus-button').click(function () {
            $(this).parent().remove();
        });
    });



    $('.chosen-select').chosen();

    var a = $('.message-box ul li .row .head');
    a.click(function () {
        if ($(this).parent().hasClass('open')) {
        } else {
            $(this).parent().parent().parent().find('.detail-content').slideToggle();
            $('html, body').animate({
                scrollTop: $('.message-box ul li.open').offset().top - 100
            }, 800);
        }
    });
    $(".lang-toggle span").click(function () {
        $(this).parent().find('ul').toggle();
    });
    $(".lang-toggle ul li a").click(function () {
        $('.lang-toggle ul li').removeClass('active');
        $(this).parent().addClass('active');
        var text = $(this).html();
        /* $(this).parent().parent().html(text);
         $(this).parent().parent().hide();*/
        $(this).parent().parent().parent().find('span').html(text);
        $(this).parent().parent().hide();
    });


    var pana = $('.lang-box .item button');
    pana.click(function () {
        if ($(this).parent().parent().parent().hasClass('open')) {
            $(this).parent().parent().parent().removeClass('open');
            $(this).prev().removeClass('open');
            $(this).parent().parent().parent().find('.edit-center').slideUp();
            $(this).parent().parent().parent().find('.text').slideUp();
            $(this).parent().parent().parent().find('.edit').slideUp();
        } else {
            pana.parent().parent().parent().find('.edit-center').slideUp();
            pana.parent().parent().parent().find('.edit').slideUp();
            pana.parent().parent().parent().find('.text').slideUp();
            pana.parent().parent().parent().removeClass('open');
            pana.prev().removeClass('open');
            $(this).parent().parent().parent().addClass('open');
            $(this).prev().addClass('open');
            $(this).parent().parent().parent().find('.edit-center').slideDown();
            $(this).parent().parent().parent().find('.text').slideDown();
            $(this).parent().parent().parent().find('.edit').slideDown();
        }
    });

    $('.radio label:nth-child(1)').click(function () {
        $(this).parent().parent().next().addClass('hidden');
    });
    $('.radio label:nth-child(3)').click(function () {
        $(this).parent().parent().next().addClass('hidden');
    });
    $('.radio label:nth-child(2)').click(function () {
        $(this).parent().parent().next().removeClass('hidden');
    });
    $('.date label:nth-child(1)').click(function () {
        $(this).parent().parent().next().addClass('hidden');
    });
    $('.date label:nth-child(2)').click(function () {
        $(this).parent().parent().next().removeClass('hidden');
    });
    $('.time label:nth-child(1)').click(function () {
        $(this).parent().find('input[type="text"]').addClass('hidden');
    });
    $('.time label:nth-child(2)').click(function () {
        $(this).parent().find('input[type="text"]').removeClass('hidden');
    });
    $('.many-times label:nth-child(1)').click(function () {
        $(this).parent().next().addClass('hidden');
    });
    $('.many-times label:nth-child(2)').click(function () {
        $(this).parent().next().addClass('hidden');
    });
    $('.many-times label:nth-child(3)').click(function () {
        $(this).parent().next().removeClass('hidden');
    });
});

$(window).load(function () {
    $('.divInput').attr("contenteditable", "true"); // Sayfa load olunca div'e düzenleme etiketini verdik.
});

function addSnippetArea(_this, _id = 0, text = "Test", placeholder = "Test", lowLimit = 55, highLimit = 65) { // Snippet area oluşturmak için.
    //var area = _this.parent().find('.select-item');
    var el_desktop;

    el_desktop = _this.parent().find('.box-list.desktop .select-item');

    var row_model = '<div class="lists" data-id="' + _id + '" data-meta-name="' + text + '"><div class="row float-none w-auto"><div class="col-sm-2"><small class="sCounter" data-limit-low="' + lowLimit + '" data-limit-high="' + highLimit + '">' + text + ' <em>0</em></small></div><div class="col-sm-8" style="padding-left: 0"><div class="divInput" spellcheck="false" contenteditable="true">' + placeholder + '</div></div><div class="col-sm-2" style="padding-left: 0;"><div class="ekle">Değişken Ekle</div></div></div></div>';

    var desktop_model = $(row_model).attr("data-meta-device", "desktop");


    $(el_desktop).append(desktop_model);

    snippetAreaLoader();
}

function removeSnippetArea(_id) { // Snippet Area silmek için kullanılabilir.
    var area = $('.select-item .lists[data-id="' + _id + '"]').remove();
}

function snippetAreaLoader() {
    $('.divInput').bind("DOMSubtreeModified", function () { // Inputta bir degisiklik olmasini dinliyor.

        var low = $(this).parent().parent().find('.sCounter').data('limit-low');
        var high = $(this).parent().parent().find('.sCounter').data('limit-high');

        var counter = $(this).parent().parent().find('.sCounter em');

        var snipCount = $(this).find('span').length;

        var deger = $(this).text().length - snipCount;

        if (deger > high) {
            counter.html(high - deger);
        } else {
            counter.html(deger);
        }

        if (deger <= low) {
            counter.css('color', 'green');
        } else if (deger <= high) {
            counter.css('color', 'orange');
        } else {
            counter.css('color', 'red');
        }

        // Inputtaki degerlerin onizleme kismina yazilmasi
        var inputtum = $(this).parent().parent().parent();
        var inputText = $(this).text().replace(new RegExp('x', 'g'), "").replace(new RegExp('', 'g'), ""); // Spanın "x" lerini temizliyor.

        if (inputtum.hasClass('stitle')) {
            inputtum.parent().parent().find('.boxes h2 i').text(inputText)
        }

        if (inputtum.hasClass('slug')) {
            inputtum.parent().parent().find('.boxes h2 span').text(inputText)
        }

        if (inputtum.hasClass('description')) {
            inputtum.parent().parent().find('.boxes p').text(inputText)
        }

    });
}

$(document).ready(function () {

    snippetAreaLoader();

    $(document).on('click', '.sModal ul li', function () { // Snippet modalında tıklanınca seçili yaptık.
        var e = $(this);
        if (e.hasClass("active")) {
            e.removeClass("active");
        } else {
            e.addClass("active");
        }
    });


    String.prototype.turkishToLower = function () { // Türkçe karakter replace.
        var string = this;
        var letters = { "İ": "i", "I": "ı", "Ş": "ş", "Ğ": "ğ", "Ü": "ü", "Ö": "ö", "Ç": "ç" };
        string = string.replace(/(([İIŞĞÜÇÖ]))/g, function (letter) { return letters[letter]; })
        return string.toLowerCase();
    }

    var editors = document.querySelectorAll(".divInput");
    for(var i = 0, l = editors.length; i < l; i++) {
        editors[i].addEventListener("paste", function(e) {
            e.preventDefault();
            var text = e.clipboardData.getData("text/plain");
            document.execCommand("insertHTML", false, text);
        });
    }

    $('.detail-metas-control-v2').delegate('.sModal button', 'click', function () { // Snippet'ı inputa yazdırmayı yaptık.

        var main = $(this).parent().parent();

        var id = main.attr('data-id');

        var device = main.attr('data-meta-device');

        var input = $('.lists[data-id="' + id + '"][data-meta-device=' + device + ']').find('.divInput');

        main.find('ul li.active').each(function () {

            var previewBox = input.closest('.detail-metas-control-v2-outer-wrapper').find('.previewBox');

            if(input.closest('.lists').data('meta-name') == 'title') {
                previewBox.find('.previewName').html(previewBox.find('.previewName').html() + $(this).data('meta-value'));
            }

            if(input.closest('.lists').data('meta-name') == 'description') {
                previewBox.find('.previewContent').html(previewBox.find('.previewContent').html() + $(this).data('meta-value'));
            }

            input.append(snippetCreat($(this).text(), $(this).data('meta-key'), $(this).data('meta-value')));

            $(this).removeClass('active');
        })

        input.find('span i').click(function () { // Tagı silmek için bir func.
            //var lists = $(this).closest(".lists")
            var previewBox = input.closest('.detail-metas-control-v2-outer-wrapper').find('.previewBox');
            if(input.closest('.lists').data('meta-name') == 'title') {
                previewBox.find('.previewName').html(previewBox.find('.previewName').html().replace($(this).parent().data('meta-value'), ''));
            }

            if(input.closest('.lists').data('meta-name') == 'description') {
                previewBox.find('.previewContent').html(previewBox.find('.previewContent').html().replace($(this).parent().data('meta-value'), ''));
            }

            $(this).parent().next().remove(); // Tagın yanındaki boşluğu siler.
            $(this).parent().remove(); // Tagın kendisini siler.
        })

        main.fadeToggle(); // Modalı kapatır.
        input.focus();

    }).delegate('.ekle', 'click', function () {
        var _id = $(this).parent().parent().parent().data('id');
        var _device = $(this).parent().parent().parent().data('meta-device');
        $('.sModal').fadeToggle().attr('data-id', _id).attr('data-meta-device', _device); // Modalı açar/kapatır.

        /*$.each(veriler, function (key, data) {
            if(data.id == _id){
                $('.sModal ul').html("");
                $.each(data.items, function (k, d) {
                    $('.sModal ul').append('<li data-value="'+d+'">'+d+'</li>');
                });
            }
        });*/
    }).delegate('.sModal input', 'input', function () {
        var main = $(this).parent().parent();
        var search = $(this).val().turkishToLower();
        var data = main.find('ul li');
        data.each(function () {
            var t = $(this);
            var name = t.data('value');
            name = name.turkishToLower();
            if (name.indexOf(search) > -1) {
                t.stop().slideDown(300);
            } else if (search == "") {
                t.stop().slideDown(300);
            } else {
                t.stop().slideUp(300);
            }
        })
    }).delegate('.addSnippetArea', 'click', function () {
        var existing_keys = [];
        var control_main = $(this).closest(".detail-metas-control-v2");
        $("input.meta_base_input[type=hidden]", control_main).each(function (ind, el) {
            let meta_key = $(el).attr("data-meta-name");
            if (existing_keys.indexOf(meta_key) == -1) {
                existing_keys.push(meta_key);
            }
        });

        const { meta_key: text } = Swal.fire({
            title: 'Meta anahtarını girin:',
            input: 'text',
            inputValue: "yeni_meta_anahtari",
            inputPlaceholder: 'Yeni oluşturulacak meta anahtarını yazın...',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            cancelButtonText: 'Vazgeç',
            //preConfirm: (meta_key)=> !(existing_keys.indexOf(meta_key.trim())+1)
        }).then((result) => {
            var meta_key = result.value;
            var ind = existing_keys.indexOf(meta_key.trim()) + 1;
            if (ind) {
                alert("Bu anahtar zaten var");
                return;
            } else {
                var url_type = $(control_main).data("url-type");
                var url_id = $(control_main).data("url-id");
                var base_input_desktop = '<input class="meta_base_input" type="hidden" value="" name="meta->fon:' + meta_key + '(url_type:' + url_type + ',url_id:' + url_id + ',key:' + meta_key + ')->desktop_value" data-meta-name="' + meta_key + '" data-meta-device="desktop" />';

                addSnippetArea($(this), 1000, result.value, "", 55, 100);
                $(control_main).append(base_input_desktop);
                //alert("Yoktu: " + result.value);
            }
        });
        //addSnippetArea($(this), 1000, "Snippet Başlığı", "Placeholder yazısı", 55, 100);
    }).delegate('.sModal .clse', 'click', function () {
        $(this).parent().parent().fadeToggle(); // Modalı açar/kapatır.
    }).delegate('.divInput', 'keydown', function (e) {
        if (e.which == 192 || e.which == 49) { // <> karakterlerini engelledik.
            e.preventDefault();
            return false;
        }
        if (e.which == 8) { // Silmeye başladığında tagı algılatıp tek seferde silecek.
            if ($(this).html().slice(-4, -1) == "pan") {
                e.preventDefault();
                $(this).find('span:last-child').remove();
            }
        }
    });

    function snippetCreat(e, k, v) { // Tag oluşturduk.
        return '<span contenteditable="false" data-meta-key="' + k + '" data-meta-value="' + v + '">' + e + '<i>x</i></span><em contenteditable="false">&nbsp;</em>';
    }


    // region DetailMetaTemplatesEditor
    $('.detail-meta-templates-editor .add-meta-key button').click(function () {
        var existing_keys = [];
        var control_main = $(this).closest(".detail-meta-templates-editor");
        var object_scope = $(control_main).data('object-scope');
        var country_group_id = $(control_main).data("country-group-id");
        var language_id = $(control_main).data("language-id");
        $("input.meta_base_input[type=hidden]", control_main).each(function (ind, el) {
            let meta_key = $(el).attr("data-meta-name");
            if (existing_keys.indexOf(meta_key) == -1) {
                existing_keys.push(meta_key);
            }
        });

        const { meta_key: text } = Swal.fire({
            title: 'Meta anahtarını girin:',
            input: 'text',
            inputValue: "yeni_meta_anahtari",
            inputPlaceholder: 'Yeni oluşturulacak meta anahtarını yazın...',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            cancelButtonText: 'Vazgeç',
            //preConfirm: (meta_key)=> !(existing_keys.indexOf(meta_key.trim())+1)
        }).then((result) => {
            var meta_key = result.value;
            var ind = existing_keys.indexOf(meta_key.trim()) + 1;
            if (ind) {
                alert("Bu anahtar zaten var");
                return;
            } else {
                var base_input_desktop = '<input class="meta_base_input" type="hidden" value="" name="meta_templates['+country_group_id+']['+language_id+'][' + object_scope + '][' + meta_key + '][desktop_value]" data-meta-name="' + meta_key + '" data-meta-device="desktop" />';

                addMetaRow($(this), meta_key, "", 55, 100);
                $(control_main).append(base_input_desktop);
                //alert("Yoktu: " + result.value);
            }
        });
        //addSnippetArea($(this), 1000, "Snippet Başlığı", "Placeholder yazısı", 55, 100);
    });

    $('.detail-meta-templates-editor').delegate(".rich-meta-input", "DOMSubtreeModified", function () { // Inputta bir degisiklik olmasini dinliyor.

        var low = $(this).parent().parent().find('.sCounter').data('limit-low');
        var high = $(this).parent().parent().find('.sCounter').data('limit-high');

        var counter = $(this).parent().parent().find('.sCounter em');
        var snipCount = $(this).find('span').length;
        var deger = $(this).text().length - snipCount;

        if (deger > high) { counter.html(high - deger); } else { counter.html(deger); }
        if (deger <= low) { counter.css('color', 'green'); } else if (deger <= high) { counter.css('color', 'orange'); } else { counter.css('color', 'red'); }

    }).delegate(".rich-meta-input", "blur", function () {
        var contents = [];
        var container = $(this).closest(".detail-meta-templates-editor");
        var device = $(this).closest(".lists").data("meta-device");
        var meta_name = $(this).closest(".lists").data("meta-name");
        var object_scope = $(container).data("object-scope");
        var country_group_id = $(container).data("country-group-id");
        var language_id = $(container).data("language-id");
        $.each($(this).contents(), function (i, e) {
            switch (e.nodeType) {
                case 1:
                    if ($(e).prop("tagName") == "SPAN") {
                        //contents.push($(e).clone().children().remove().end().text());
                        contents.push("%" + $(e).data("meta-key") + "%");
                    }
                    break;
                case 3:
                    //javascript remove new lines: .replace(/\\r?\\n|\\r/gm,"")
                    //javascript replace &nbsp; s : .replace(/\u00a0/g, " ")
                    contents.push(e.nodeValue.replace(/\u00a0/g, " ").replace(/\\r?\\n|\\r/gm, ""));
                    break;
                default:
                    break;
            }
        });
        // Reduce multiple spaces to one: s.replace(/ +(?= )/g,'')
        var content = contents.join("").trim().replace(/ +(?= )/g, '');
        var hidden_input = $("input.meta_base_input[type=hidden][data-meta-name=" + meta_name + "][data-meta-device=" + device + "]", container);
        if (!hidden_input.length) {
            var name_prefix = $(container).data("field-name-prefix");
            $(container).append('<input type="hidden" class="meta_base_input" name="meta_templates['+country_group_id+']['+language_id+'][' + object_scope + '][' + meta_name + "][" + meta_device + '_value]" value="' + content + '" data-meta-name="' + meta_name + '" data-meta-device="' + meta_device + '">');
        } else {
            hidden_input.val(content);
        }

    }).delegate(".rich-meta-input", "DOMSubtreeModified", function () { // Inputta bir degisiklik olmasini dinliyor.

        var low = $(this).parent().parent().find('.sCounter').data('limit-low');
        var high = $(this).parent().parent().find('.sCounter').data('limit-high');

        var counter = $(this).parent().parent().find('.sCounter em');
        var snipCount = $(this).find('span').length;
        var deger = $(this).text().length - snipCount;

        if (deger > high) { counter.html(high - deger); } else { counter.html(deger); }
        if (deger <= low) { counter.css('color', 'green'); } else if (deger <= high) { counter.css('color', 'orange'); } else { counter.css('color', 'red'); }

    }).delegate(".rich-meta-input", "keydown", function () {
        if (e.which == 192 || e.which == 49) { // <> karakterlerini engelledik.
            e.preventDefault();
            return false;
        }
        if (e.which == 8) { // Silmeye başladığında tagı algılatıp tek seferde silecek.
            if ($(this).html().slice(-4, -1) == "pan") {
                e.preventDefault();
                $(this).find('span:last-child').remove();
            }
        }
    }).delegate('.add-variable', 'click', function () {

        var main = $(this).closest('.detail-meta-templates-editor');
        var meta_key = $(this).closest('.lists').data('meta-name');
        var device = $(this).closest('.lists').data('meta-device');
        $('.meta-variables-modal', main).fadeToggle().attr('data-meta-name', meta_key).attr('data-meta-device', device); // Modalı açar/kapatır.

    }).delegate('.meta-variables-modal ul li', 'click', function () { // Snippet modalında tıklanınca seçili yaptık.

        $(this).toggleClass("active");

    }).delegate('.meta-variables-modal .clse', 'click', function () {

        $(this).closest('.meta-variables-modal').fadeToggle(); // Modalı açar/kapatır.

    }).delegate('.meta-variables-modal input', 'input', function () {

        var modal = $(this).closest('.meta-variables-modal');
        var search = $(this).val().turkishToLower();
        var data = modal.find('ul li');
        data.each(function () {
            var t = $(this);
            var name = t.data('value');
            name = name.turkishToLower();
            if (name.indexOf(search) > -1) {
                t.stop().slideDown(300);
            } else if (search == "") {
                t.stop().slideDown(300);
            } else {
                t.stop().slideUp(300);
            }
        })

    }).delegate('.meta-variables-modal button', 'click', function () { // Snippet'ı inputa yazdırmayı yaptık.

        var modal = $(this).closest('.meta-variables-modal');
        var main = $(modal).closest('.detail-meta-templates-editor');
        var meta_key = modal.attr('data-meta-name');
        var device = modal.attr('data-meta-device');
        var input = $('.lists[data-meta-name="' + meta_key + '"][data-meta-device=' + device + '] .rich-meta-input', main);

        modal.find('ul li.active').each(function () {
            input.append(createMetaVarBox($(this).text(), $(this).data('meta-key'), $(this).data('meta-value')));
            $(this).removeClass('active');
        })


        modal.attr('data-meta-name', '').attr('data-meta-device', '').fadeToggle(); // Modalı kapatır.
        input.focus();

    }).delegate('.rich-meta-input span>i', 'click', function () {
        $(this).parent().next().remove(); // Tagın yanındaki boşluğu siler.
        $(this).parent().remove(); // Tagın kendisini siler.
    });





    // endregion



});

// region DetailMetaTemplatesEditor
function addMetaRow(_this, text = "Test", placeholder = "Test", lowLimit = 55, highLimit = 65) { // Snippet area oluşturmak için.

    var el_desktop, el_mobile;

    var control_main = $(_this).closest('.detail-meta-templates-editor');

    el_desktop = $('.box-list.desktop .select-item', control_main);


    var row_model = '<div class="lists" data-meta-name="' + text + '">' +
        '<div class="row float-none w-auto">' +
        '<div class="col-sm-2">' +
        '<small class="sCounter" data-limit-low="' + lowLimit + '" data-limit-high="' + highLimit + '">' + text + ' <em>0</em></small>' +
        '</div>' +
        '<div class="col-sm-8" style="padding-left: 0">' +
        '<div class="rich-meta-input" spellcheck="false" contenteditable="true">' + placeholder + '</div>' +
        '</div>' +
        '<div class="col-sm-2" style="padding-left: 0;">' +
        '<div class="add-variable">Değişken Ekle</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    var desktop_model = $(row_model).attr("data-meta-device", "desktop");


    $(el_desktop).append(desktop_model);
}

function createMetaVarBox(e, k, v) {
    return '<span contenteditable="false" data-meta-key="' + k + '" data-meta-value="' + v + '">' + e + '<i>x</i></span><em contenteditable="false">&nbsp;</em>';
}

// endregion

function htmlspecialchars(str) {
    if (typeof (str) == "string") {
        str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
        str = str.replace(/"/g, "&quot;");
        str = str.replace(/'/g, "&#039;");
        str = str.replace(/</g, "&lt;");
        str = str.replace(/>/g, "&gt;");
    }
    return str;
}

function rhtmlspecialchars(str) {
    if (typeof (str) == "string") {
        str = str.replace(/&gt;/ig, ">");
        str = str.replace(/&lt;/ig, "<");
        str = str.replace(/&#039;/g, "'");
        str = str.replace(/&quot;/ig, '"');
        str = str.replace(/&amp;/ig, '&'); /* must do &amp; last */
    }
    return str;
}
