Highcharts.chart('platform-pie', {

    title: {
        text: '',
        align: 'center'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    legend: {
        align: 'center',
        verticalAlign: 'bottom',
        itemMarginBottom: 8,
        useHTML: true,
        labelFormatter: function () {
            var legendItem = document.createElement('div'),
                symbol = document.createElement('span'),
                label = document.createElement('span');

            symbol.innerText = this.y.toFixed(0);
            symbol.style.borderColor = this.color;
            symbol.classList.add('percent');
            label.classList.add('browserText');
            legendItem.classList.add('rightChartText');
            label.innerText = this.name;

            legendItem.appendChild(symbol);
            legendItem.appendChild(label);

            return legendItem.outerHTML;
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        innerSize: '50%',
        size: '90%',
        borderWidth: 0,
        data: window.graphData.platform
    }]
});


Highcharts.chart('referer-pie', {

    title: {
        text: '',
        align: 'center'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    legend: {
        align: 'center',
        verticalAlign: 'bottom',
        itemMarginBottom: 8,
        useHTML: true,
        labelFormatter: function () {
            var legendItem = document.createElement('div'),
                symbol = document.createElement('span'),
                label = document.createElement('span');

            symbol.innerText = this.y.toFixed(0);
            symbol.style.borderColor = this.color;
            symbol.classList.add('percent');
            label.classList.add('browserText');
            legendItem.classList.add('rightChartText');
            label.innerText = this.name;

            legendItem.appendChild(symbol);
            legendItem.appendChild(label);

            return legendItem.outerHTML;
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        innerSize: '50%',
        borderWidth: 0,
        data: window.graphData.referers
    }]
});

