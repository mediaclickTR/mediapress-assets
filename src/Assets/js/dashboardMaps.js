// Initiate the chart
Highcharts.mapChart('container', {
    chart: {
        map: 'custom/world',
        borderWidth: 0
    },

    colors: ['rgba(19,64,117,0.05)', 'rgba(19,64,117,0.2)', 'rgba(19,64,117,0.4)',
        'rgba(19,64,117,0.5)', 'rgba(19,64,117,0.6)', 'rgba(19,64,117,0.8)', 'rgba(19,64,117,1)'],

    title: {
        text: ''
    },

    mapNavigation: {
        enabled: false
    },

    legend: {
        title: {
            text: 'Ziyaret Yoğunluğu',
            style: {
                color: ( // theme
                    Highcharts.defaultOptions &&
                    Highcharts.defaultOptions.legend &&
                    Highcharts.defaultOptions.legend.title &&
                    Highcharts.defaultOptions.legend.title.style &&
                    Highcharts.defaultOptions.legend.title.style.color
                ) || 'black'
            }
        },
        align: 'left',
        verticalAlign: 'bottom',
        floating: true,
        layout: 'vertical',
        valueDecimals: 0,
        backgroundColor: ( // theme
            Highcharts.defaultOptions &&
            Highcharts.defaultOptions.legend &&
            Highcharts.defaultOptions.legend.backgroundColor
        ) || 'rgba(255, 255, 255, 0.85)',
        symbolRadius: 0,
        symbolHeight: 14
    },

    colorAxis: {
        dataClasses: [{
            to: 3
        }, {
            from: 3,
            to: 10
        }, {
            from: 10,
            to: 30
        }, {
            from: 30,
            to: 100
        }, {
            from: 100,
            to: 300
        }, {
            from: 300,
            to: 1000
        }, {
            from: 1000,
            to: 10000
        }, {
            from: 10000,
            to: 100000
        },
            {
                from: 100000,
            }
        ]
    },

    series: [{
        data: window.graphData.countries,
        joinBy: ['iso-a2', 'code'],
        animation: true,
        name: 'Ziyaret Sayısı',
        states: {
            hover: {
                color: '#008dce'
            }
        },

        shadow: false
    }]
});


